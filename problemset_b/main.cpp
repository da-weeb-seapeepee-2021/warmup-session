#include <iostream>
#include <string>
#include <sstream>

using namespace std;

long long int factorial(long long int n){
    long long int result = 1;

    for (long long int i = 1 ; i <= n; i++){
        result *= i;
    }

    return result;
}

long long int factorial(long long int n, long long int k){
    long long int result = 1, target = n + k;

    for (long long int i = target ; i > n; i--){
        result *= i;
    }

    return result;
}


int main() {
    string line_reader;
    long long int prizes, minimum, participants = 0;

    cin >> prizes;
    cin.get();
    getline(cin, line_reader);
    istringstream values(line_reader);

    while (values >> minimum){
        prizes -= minimum;
        participants++;
    }

    if (prizes == 0) {
        cout << 1 << endl;
    }

    if (prizes > 0 && prizes < participants) {
        cout << prizes * participants << endl;
    }

    if (prizes > 0 && prizes >= participants) {
        cout << (factorial(prizes, participants - 1) / factorial(participants - 1)) % 1000000007 << endl;
    }

    return 0;
}
