#include <iostream>
#include <string>
#include <algorithm>

int main() {

    int high = 0, low = 0, repeat = 0, paper = 0;
    std::cin >> paper;
    std::string result;

    for (int i = 0; i < paper; i++) {
        std::cin >> high >> low >> repeat;

        for (int j = 0; j < repeat; j++) {
            high /= 2;

            if (low > high) {
                std::swap(high, low);
            }
        }

        result += std::to_string(high) + ' ' + std::to_string(low) + '\n';
    }

    for (auto &i : result) {
        std::cout << i;
    }

    return 0;
}

