#include <iostream>
#include <math.h>
#include <vector>

using namespace std;

int main() {
    long long int A_value, B_value, result=0;
    int cases;
    vector<int> results;
    double rt;
    cin >> cases;
    for (int i=0 ; i < cases ; i++){
        cin >> A_value >> B_value;
        for (int d=2, n=A_value+1 ; n<=B_value; d=2, n++){
            for (rt = sqrt(n); d < rt ; d++){
                if (n % d == 0){
                    break;
                }
            }
            if (d > rt) {
                result++;
            }
        }
        results.push_back(result);
        result = 0;
    }

    for (auto i = results.begin(); i != results.end(); ++i){
        cout << *i << endl;
    }
    return 0;
}
