cmake_minimum_required(VERSION 3.19)
project(blank_cpp14_executable)

set(CMAKE_CXX_STANDARD 14)

add_executable(blank_cpp14_executable main.cpp)