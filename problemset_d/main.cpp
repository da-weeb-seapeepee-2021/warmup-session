#include <iostream>

using namespace std;

int main() {
    unsigned long long int min, max, galih_value, galuh_value, gilang_value, unique=0;
    int pass_state = 0;
    cin >> min >> max >> galih_value >> galuh_value >> gilang_value;
    for (int i=min ; i<=max ; i++){
        if (i % galih_value == 0){
            pass_state++;
        }
        if (i % galuh_value == 0){
            pass_state++;
        }
        if (i % gilang_value == 0){
            pass_state++;
        }

        if (pass_state == 1){
            unique++;
        }

        pass_state = 0;
    }
    cout << unique << endl;
}
