#include <iostream>
#include <vector>

int main() {

    long long int range_low, range_high, value_a, value_b, value_c, inclusive_counter = 0;

    std::cin >> range_low >> range_high;
    std::cin >> value_a >> value_b >> value_c;
    std::vector<long long int> sorted_base {value_a, value_a, value_a};

    if (value_b > sorted_base[2]) {
        sorted_base[2] = value_b;
    }

    if (value_b < sorted_base[0]) {
        sorted_base[0] = value_b;
    }

    if (value_c > sorted_base[0]) {
        sorted_base[1] = value_c;
    }

    if (value_c > sorted_base[2]) {
        sorted_base[1] = sorted_base[2];
        sorted_base[2] = value_c;
    }

    if (value_c < sorted_base[0]) {
        sorted_base[1] = sorted_base[0];
        sorted_base[0] = value_c;
    }

    for (int i = 2; i >= 0; i--) {
        long long int low_div, high_div, copy_range_low = range_low, copy_range_high = range_high;

        while (copy_range_low % sorted_base[i] != 0) {
            copy_range_low++;
        }

        while (copy_range_high % sorted_base[i] != 0) {
            copy_range_high--;
        }

        low_div = copy_range_low / sorted_base[i];
        high_div = copy_range_high / sorted_base[i];

        while (low_div <= high_div) {
            bool inclusive_state = true;

            for (auto &m : sorted_base) {
                if (m != sorted_base[i]) {
                    if ((sorted_base[i] * low_div) % m == 0) {
                        inclusive_state = false;
                    }
                }
            }

            if (inclusive_state) {
                inclusive_counter++;
            }

            low_div++;
        }
    }

    std::cout << inclusive_counter << std::endl;

    return 0;
}
