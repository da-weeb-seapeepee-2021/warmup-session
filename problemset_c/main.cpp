#include <iostream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

vector<int> element_split(int size, bool first_run, int offset = 0) {
    int local_counter = 0, local_value;
    vector<int> local_array;
    string line_reader;

    local_array.resize(size);

    if (first_run) {
        cin.get();
    }

    getline(cin, line_reader);

    istringstream string_stream(line_reader);

    while (string_stream >> local_value) {
        local_array[local_counter] = local_value - offset;
        local_counter++;
    }

    return local_array;
}

int main() {

    int size_x, size_y;
    long int cases;
    vector<vector<int>> matrix, sub_matrix;

    cin >> size_x >> size_y >> cases;

    matrix.resize(size_x);
    sub_matrix.resize(cases);

    for (int i = 0; i < size_x ;i++) {
        matrix[i].resize(size_y);
        matrix[i] = element_split(size_y, (i == 0));
    }

    for (int i = 0; i < cases ;i++) {
        sub_matrix[i].resize(4);
        sub_matrix[i] = element_split(4, false, 1);
    }

    for (int i = 0; i < sub_matrix.size(); i++) {
        int temp_count = 0;

        for (int k = sub_matrix[i][0]; k <= sub_matrix[i][2]; k++) {
            for (int m = sub_matrix[i][1]; m <= sub_matrix[i][3]; m++) {
                temp_count += matrix[k][m];
            }
        }

        cout << temp_count << endl;
    }

    return 0;
}

